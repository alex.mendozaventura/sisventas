﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//librerias
using Entities;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
namespace DataAccess
{
    public class DCategoria
    {
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlConex"].ConnectionString);

        public int InsertarCategoria(Categoria entiDto)
        {
            int result = 0;
            try
            {

                SqlCommand cmd = new SqlCommand("PA_INSERTARCATEGORIA", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pcNombre", entiDto.Nombre);
                cmd.Parameters.AddWithValue("@pcDescripcion", entiDto.Descripcion);

                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }

                cn.Open();

                result = cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        public int ActualizarCategoria(Categoria entiDto) {
            int resultado = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("PA_ACTUALIZARCATEGORIA",cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pnIdCategoria", entiDto.CodigoCategoria);
                cmd.Parameters.AddWithValue("@pcNombre", entiDto.Nombre);
                cmd.Parameters.AddWithValue("@pcDescripcion", entiDto.Descripcion);
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cn.Open();
                resultado = cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception)
            {

                throw;
            }
            return resultado;
        }
        public int EliminarCategoria(Categoria entiDto) {
            int resultado = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("PA_ELIMINARCATEGORIA", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pnIdCategoria", entiDto.CodigoCategoria);

                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }

                cn.Open();
                resultado = cmd.ExecuteNonQuery();
                cn.Close();

            }
            catch (Exception)
            {

                throw;
            }

            return resultado;
        }
        public DataTable ListarCategoria()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("PA_LISTARCATEGORIA", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable BuscarCategoria(Categoria entiDto)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("PA_LISTARCATEGORIA", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pcNombre", entiDto.Nombre);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
