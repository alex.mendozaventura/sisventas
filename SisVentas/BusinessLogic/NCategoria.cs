﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Referencias
using DataAccess;
using Entities;
using System.Data;

namespace BusinessLogic
{
    public class NCategoria
    {
        DCategoria _dCategoria = new DCategoria();
        public int InsertarCategoria(Categoria entiDto)
        {
          return  _dCategoria.InsertarCategoria(entiDto);
        }
        public int ActualizarCategoria(Categoria entiDto)
        {
            return _dCategoria.ActualizarCategoria(entiDto);
        }
        public int EliminarCategoria(Categoria entiDto)
        {
            return _dCategoria.EliminarCategoria(entiDto);
        }
        public DataTable ListarCategoria() {
            return _dCategoria.ListarCategoria();
        }
        public DataTable BuscarCategoria(Categoria entiDto)
        {
            return _dCategoria.BuscarCategoria(entiDto);
        }
    }
}
