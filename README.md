# Sistema de Ventas

## Funcionalidades
### Gestión de Accesos:
Registro de usuarios y gestion de acceso según restricciones por tipo de usuario.
### Gestión de Clientes:
Conserva la información de tus clientes en una base de datos, edita, borra y busca con facilidad.
### Gestión de Proveedores:
Conversa la información de tus proveedores, datos comerciales y legales, edita, borra y busca con facilidad.
### Gestión de Ventas:
Realiza pedidos, ventas y salidas de almacén, seguimiento de artículos, flujo de caja.
### Reporte de Ventas:
Realiza aportes diarias, mensuales o dependiendo de las necesidades, por producto o por cliente para llevar un mejor control y tomar mejores decisiones, repote de utilidades que generan nuestras ventas, kardex físico valorizado, ventas realizadas por vendedor, seguimiento de artículos, reporte de unidades en existencias.

## Caracteristícas
## Lenguaje de Programación C# .Net
C# es un lenguaje de programación orientado a objetos desarrollado y estandarizado por Microsoft como parte de su plataforma .Net. Utilizaremos el IDE Microsoft Visual Studio 2015.
## SQL Server 2014
Microsoft SQLServer es un sistema para la Gestión de Base de Datos basado en el Modelo Relacional.
